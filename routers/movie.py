from fastapi import APIRouter
from fastapi import Path, Query#, Depends
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
#from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()
        
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200)#, dependencies=[Depends(JWTBearer())]
def getMovies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)#, dependencies=[Depends(JWTBearer())]
def getMovieById(id: int = Path(ge = 1, le = 2000)) -> Movie: # Validación Para Parametro Path
    db = Session()
    result = MovieService(db).get_movies(id)
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)#, dependencies=[Depends(JWTBearer())]
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)#, dependencies=[Depends(JWTBearer())]
def getMoviesByCategory(category: str = Query(min_length=1, max_length=15)) -> List[Movie]: # Validación Parametro Query
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.category == category).all()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.post('/movies/', tags=['movies'], response_model=dict, status_code=200) #, dependencies=[Depends(JWTBearer())]
def create_movie(movie: Movie) -> dict:
    db = Session()
    next_id = db.query(MovieModel).count() + 1
    
    while db.query(MovieModel).filter(MovieModel.id == next_id).first() is not None:
        next_id += 1

    movie.id = next_id
    MovieService(db).create_movie(movie)

    return JSONResponse(content={"message": "Movie Successfully Created"}, status_code=200)
    
@movie_router.delete('/movies/{id}', tags=['movies'], status_code=200)#, dependencies=[Depends(JWTBearer())]
def deleteMovie(id: int):
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    db.delete(result)
    db.commit()

@movie_router.put('/movies/{id}', tags=['movies'], status_code=200)#, dependencies=[Depends(JWTBearer())]
def updateMovie(id: int, movie: Movie):
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    
    # keys = list(dict(movie).keys())
    # for item in keys:
    #     result. = movie.item
    result.title = movie.title
    result.overview = movie.overview
    result.year = movie.year
    result.rating = movie.rating
    result.category = movie.category
    db.commit()
    return JSONResponse(content={"message": "Movie Succesfully Updated"}, status_code=200)