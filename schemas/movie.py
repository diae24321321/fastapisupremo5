from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None # varible opcional
    title: str = Field(max_length = 15, min_length = 1)
    overview: str = Field()
    year: int = Field(le = 2024)
    rating: float = Field(ge = 0, le = 10)
    category: str = Field(min_length = 1, max_length = 15)
    
    class Config:
        json_schema_extra = {
            "example": {
                "id": 0,
                "title": "Mi Movie",
                "overview": "Desc",
                "year": 2000,
                "rating": 6.6,
                "category": "Romance"
            }
        }