from pydantic import BaseModel, Field
from typing import Optional

class User(BaseModel):
    email: str
    password: str
    
    class Config:
        json_schema_extra = {
            "example": {
                "email": "admin@gmail.com",
                "password": "admin",
            }
        }