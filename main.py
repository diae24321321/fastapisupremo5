from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
#from routers.user import user_router
from routers.computer import computer_router

app = FastAPI()  
app.title = "Movies And Computers Manager"
app.version = "1.0"

app.add_middleware(ErrorHandler) # agregamos middleware al app
#app.include_router(user_router, tags='Xd')
app.include_router(computer_router)
app.include_router(movie_router)

Base.metadata.create_all(bind=engine) # crea archivo de db que contiene las tablas

@app.get('/', tags=['home']) #Creación de End Point
def message():
    return HTMLResponse('<h1> Bienvenidos a nuestra app en linea </h1>')